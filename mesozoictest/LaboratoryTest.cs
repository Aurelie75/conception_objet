using System;
using Xunit;

using Mesozoic;

namespace mesozoictest
{
    public class LaboratoryTest
    {
        [Fact]
        public void testcreateDinosaur()
        {
            Dinosaur stitch = new Dinosaur("stitch", "extraterrestre", 5);
            Assert.Equal("Stitch", stitch.GetName());
            Assert.Equal("extraterrestre", stitch.GetSpecie());
            Assert.Equal(5, stitch.GetAge());

        }
    }

}