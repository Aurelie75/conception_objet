using System;
using Xunit;

using Mesozoic;

namespace mesozoictest
{
    public class HordeTest
    {
        [Fact]
        public void TestAddDinosaur()
        {
            Horde horde = new Horde();
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
            Dinosaur nessie = new Dinosaur("Nessie", "Diplodocus", 11);
            Assert.Empty(horde.GetDinosaurs());
            horde.ajouterdino(louis);
            Assert.Single(horde.GetDinosaurs());
            Assert.Equal(louis, horde.GetDinosaurs()[0]);
            horde.ajouterdino(nessie);
            Assert.Equal(2, horde.GetDinosaurs().Count);
            Assert.Equal(nessie, horde.GetDinosaurs()[1]);
        }

        [Fact]
        public void TestRemoveDinosaur()
        {
            Horde horde = new Horde();
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
            Dinosaur nessie = new Dinosaur("Nessie", "Diplodocus", 11);
            horde.ajouterdino(louis);
            horde.ajouterdino(nessie);

            Assert.Equal(2, horde.GetDinosaurs().Count);
            horde.supprimerdino(louis);
            Assert.Single(horde.GetDinosaurs());
            horde.supprimerdino(nessie);
            Assert.Empty(horde.GetDinosaurs());
        }


        [Fact]
        public void Testpresentationall()
        {
            Horde horde = new Horde();
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
            Dinosaur nessie = new Dinosaur("Nessie", "Diplodocus", 11);
            horde.ajouterdino(louis);
            horde.ajouterdino(nessie);

            string expected_introduction = "Je suis Louis le Stegausaurus, j'ai 12 ans.\nJe suis Nessie le Diplodocus, j'ai 11 ans.\n";

            Assert.Equal(expected_introduction, horde.presentationall());
        }
    }
}