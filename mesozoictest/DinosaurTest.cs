using System;
using Xunit;

using Mesozoic;


namespace mesozoictest
{
    public class DinosaurTest
    {
        
        [Fact]
        public void TestDinosaurConstructor()
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);

            Assert.Equal("Louis", louis.GetName());
            Assert.Equal("Stegausaurus", louis.GetSpecie());
            Assert.Equal(12, louis.GetAge());
        }

        [Fact]
        public void TestDinosaurRoar()
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
            Assert.Equal("Grrr", louis.roar());
        }

        [Fact]
        public void TestDinosaurSayHello()
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
            Assert.Equal("Je suis Louis le Stegausaurus, j'ai 12 ans.", louis.sayHello());
        }

        [Fact]
        public void TestDinosaurhug()
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
            Dinosaur Nessie = new Dinosaur("Nessie","Diplodocus", 11);
            Assert.Equal("Je suis Louis et je fais un calin à Nessie", louis.hug(Nessie));
        }
    }
}
