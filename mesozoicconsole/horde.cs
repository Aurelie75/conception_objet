using System.Text;
using Mesozoic;
using System.Collections.Generic;

namespace Mesozoic
{
    public class Horde
    {
        private List<Dinosaur> horde;

        public Horde()
        {
            this.horde = new List<Dinosaur>();
        }

        public void ajouterdino (Dinosaur Dino)
        {
            this.horde.Add(Dino);
        }

        public void supprimerdino (Dinosaur Dino)
        {
            this.horde.Remove(Dino);
        }

        public string presentationall()
        {
            StringBuilder introduce_builder = new StringBuilder();
            foreach(Dinosaur dinosaur in this.horde)
            {
                introduce_builder.AppendFormat("{0}\n", dinosaur.sayHello());
            }
            return introduce_builder.ToString().Trim();
           
        }

        public List<Dinosaur> GetDinosaurs()
        {
            return this.horde;
        }

    }
}