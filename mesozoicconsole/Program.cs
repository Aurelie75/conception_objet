﻿using System;
using Mesozoic;
using System.Collections.Generic;


namespace mesozoicconsole
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            Dinosaur Sunday = new Dinosaur("Sunday","Felinaurus", 4);
            Dinosaur Doudou = new Dinosaur("Doudou","Sourisaur", 21);
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
            Dinosaur nessie = new Dinosaur("Nessie", "Diplodocus", 11);

            Console.WriteLine(Sunday.GetSpecie());
            Console.WriteLine(Doudou.sayHello());
            Console.WriteLine(Doudou.hug(Sunday));

            List<Dinosaur> dinosaurs = new List<Dinosaur>();

            dinosaurs.Add(louis); //ajoute un dinosaur a la fin de la liste
            dinosaurs.Add(nessie);
            dinosaurs.Add(Sunday);
            dinosaurs.Add(Doudou);

            Console.WriteLine(dinosaurs.Count);
            //Iterate over our list
            foreach (Dinosaur dino in dinosaurs)
            {
                Console.WriteLine(dino.GetName());
            }

            dinosaurs.RemoveAt(1); //Remove dinosaur at index 1

            Console.WriteLine(dinosaurs.Count);
            //Iterate over our list
            foreach (Dinosaur dino in dinosaurs)
            {
                Console.WriteLine(dino.sayHello());
            }

            dinosaurs.Remove(louis);

            Console.WriteLine(dinosaurs.Count);
            //Iterate over our list
            foreach (Dinosaur dino in dinosaurs)
            {
                Console.WriteLine(dino.GetName());
            }

            Console.WriteLine("\nCréation d'un troupeau");

            Horde horde = new Horde();
            horde.ajouterdino(louis);
            horde.ajouterdino(nessie);

            Console.WriteLine(horde.presentationall());

            Dinosaur henry = new Dinosaur("Henry","Diplodocus", 11);
            Console.WriteLine(henry.sayHello());
            Dinosaur stitch = new Dinosaur("Stitch", "Extraterrestre", 12);
            Console.WriteLine(stitch.sayHello());
            Console.WriteLine(henry.sayHello());

        }
    }
}
