using System;

namespace Mesozoic
{
    public class Diplodocus : Dinosaur 
{
    public Diplodocus (string name, int age): base (name, age)
    {

    }
    protected override string Specie { get { return "Diplodocus"; } }
}
}