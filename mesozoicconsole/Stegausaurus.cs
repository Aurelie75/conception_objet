using System;

namespace Mesozoic
{
    public class Stegausaurus : Dinosaur 
{
    public Stegausaurus (string name, int age): base (name, age)
    {

    }
    protected override string Specie { get { return "Stegausaurus"; } }
    //Encore une fois la classe est tronquée
}
}