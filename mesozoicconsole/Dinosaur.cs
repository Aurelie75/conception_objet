using System;

namespace Mesozoic
{
    public abstract class Dinosaur
    {
        protected string name;
        protected virtual string Specie;
        protected int age;


        public Dinosaur(string name, int age)
        {
            this.name =name;
            this.age = age;
        }

        public string sayHello()
        {
            return string.Format("Je suis {0} le {1}, j'ai {2} ans.",this.name, Dinosaur.specie,this.age);
        }

        public string roar()
        {
            return "Grrr";
        }

        public string GetName()
        {
            return this.name;
        }
         public string GetSpecie()
        {
            return Dinosaur.specie;
        }
         public int GetAge()
        {
            return this.age;
        }
         public void SetName(string name)
        {
            this.name= name ;
        }
        public string hug(Dinosaur dino)
        {
            return string.Format("Je suis {0} et je fais un calin à {1}",this.name, dino.name);
        }
    }
}