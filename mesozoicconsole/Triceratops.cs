using System;

namespace Mesozoic
{
    public class Triceratops : Dinosaur 
{
    public Triceratops (string name, int age): base (name, age)
    {

    }
    protected override string Specie { get { return "Triceratops"; } }
    //Encore une fois la classe est tronquée
}
}